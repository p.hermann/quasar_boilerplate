export default {
    path: 'administration',
    component: () => import('layouts/GuardLayout'),
    meta: {
      breadcrumbs: 'Administration',
      menu: 'Administration',
      icon: 'la la-user-shield',
    },
    children: [
      {
        path: 'users',
        component: () => import('layouts/GuardLayout'),
        meta: {
          breadcrumbs: 'Utilisateurs',
        },
        children: [ {
            path: '',
            name: 'users',
            component: () => import('pages/administration/users/index'),
            meta: {
              menu: 'Utilisateurs',
              icon: '',
              // permissions: ['menu_web_user']
            },
          },
          {
            path: 'form/:type(update|view|create)/:id?',
            name: 'users_form',
            component: () => import('pages/administration/users/form'),
            meta: {
              breadcrumbs: 'Détails',
              // permissions: ['show_users', 'index_users']
            }
          },
        ]
      },
      {
        path: 'roles',
        component: () => import('layouts/GuardLayout'),
        meta: {
          breadcrumbs: 'Profiles',
        },
        children: [
          {
            path: '',
            name: 'roles',
            component: () => import('pages/administration/roles/index'),
            meta: {
              menu: 'Profiles',
              subTitle: '(Roles)',
              icon: '',
              // permissions: ['menu_web_role']
            }
          },
          {
            path: 'form/:type',
            name: 'roles_form',
            component: () => import('pages/administration/roles/form'),
            meta: {
              breadcrumbs: 'Détails profile',
            }
          },
        ]
      },
      {
        path: 'permissions',
        name: 'permissions',
        component: () => import('pages/administration/permissions/index'),
        meta: {
          breadcrumbs: 'Permissions',
          menu: 'Permissions',
          icon: '',
          // permissions: ['menu_web_permission']
        }
      },
      {
        path: 'parameters',
        component: () => import('layouts/GuardLayout'),
        meta: {
          breadcrumbs: 'Paramètres',
        },
        children: [
          {
            path: '',
            name: 'parameters',
            component: () => import('pages/administration/parameters/index'),
            meta: {
              menu: 'Paramètres',
              icon: '',
              // permissions: ['menu_web_employe']
            }
          },
          {
            name: 'parameters-form',
            path: 'form/:type',
            component: () => import('pages/administration/parameters/form'),
            meta: {
              breadcrumbs: 'Détails',
              // permissions: ['index_employees', 'show_employees']
            }
          },
        ]
      },
    ]
}