export default [
  {
    path: '/login',
    component: () => import('layouts/AuthsLayout'),
    children: [
      {
        name: 'login',
        path: '',
        component: () => import('pages/authentication/Login')
      },
    ]
  },

  // {
  //   name: 'changePassword',
  //   path: '/change-password',
  //   component: () => import('pages/Auth/ChangePassword')
  // },
  // {
  //   name: 'forgetPassword',
  //   path: '/forget-password',
  //   component: () => import('pages/Auth/ForgetPassword')
  // }
]
