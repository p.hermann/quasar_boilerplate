import {mergeRoute} from 'src/helpers/utilities'
import authentication from './authentication.routes'
import administration from './administration.routes'
import tasksRoutes from './tasks.routes.js'

const defaultRoutes = [{
  name: 'error-404',
  path: '*',
  component: () => import('pages/errors/404.vue')
}]

// const applicationRoutes = mergeRoute(dashboardRoutes, staff, settings, administration)
const applicationRoutes = mergeRoute(administration, tasksRoutes);

const routes = [
  ...authentication,
  {
    path: '/',
    component: () => import('layouts/AppsLayout'),
    children: applicationRoutes,
  },
  ...defaultRoutes,
]

export default routes

export {applicationRoutes}
