export default [
    {
      path: '',
      name: 'tasks',
      component: () => import('pages/tasks/index.vue'),
      meta: {
        breadcrumbs: "Themes",
        menu: 'Themes',
        icon: 'la la-user-shield',
      }
    },
    {
      path: '/tasks',
      name: 'tasks',
      component: () => import('pages/tasks/index.vue'),
      meta: {
        breadcrumbs: "Configurations",
        menu: 'Configurations',
        icon: 'la la-cogs',
      }
    },
    {
      path: '/tasks',
      name: 'tasks',
      component: () => import('pages/tasks/index.vue'),
      meta: {
        breadcrumbs: "Securite",
        menu: 'Securite',
        icon: 'la la-lock',
      }
    },
    {
      path: '/tasks',
      name: 'tasks',
      component: () => import('pages/tasks/index.vue'),
      meta: {
        breadcrumbs: "Messages",
        menu: 'Messages',
        icon: 'la la-inbox',
      }
    },
    {
      path: '/tasks',
      name: 'tasks',
      component: () => import('pages/tasks/index.vue'),
      meta: {
        breadcrumbs: "Deconnexion",
        menu: 'Deconnexion',
        icon: 'la la-sign-out-alt',
      }
    },
]
