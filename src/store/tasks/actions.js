import api from 'src/repositories/tasks'

export function index({commit,dispatch}, payload) {
    return api.index(payload).then(async ({data}) => {
      commit('setTasks',{ tasks: data });
      return data
    })
}