export default function () {
    return {
      loading: false,
      items: [
        {
          id: 1,
          title: "Title 01",
          completed: true
        },
        {
          id: 2,
          title: "Title 02",
          completed: false
        },
        {
          id: 3,
          title: "Title 03",
          completed: true
        },
        {
          id: 4,
          title: "Title 04",
          completed: true
        },
        {
          id: 5,
          title: "Title 05",
          completed: false
        },
        {
          id: 6,
          title: "Title 06",
          completed: false
        },
      ]
    }
  }
