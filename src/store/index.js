import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'

//Authentication
import auth from './authentication/auth'

//Administration
import users from './administration/users'
import roles from './administration/roles'
import permissions from './administration/permissions'
import application from './administration/application'
import parameters from './administration/parameters'

//Tasks
import tasks from './tasks'

Vue.use(Vuex)
/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */
export default function (/* { ssrContext } */) {
  const authPersist = new VuexPersistence({
    key: 'grh_auth',
    storage: window.localStorage,
    modules: ['auth']
  })

  const appPersist = new VuexPersistence({
    key: 'grh_app',
    storage: window.localStorage,
    reducer: state => ({

    })
  })

  return new Vuex.Store({
    modules: {
      //Authentication
      auth,

      //Administration
      users,
      permissions,
      application,
      roles,
      parameters,

      //Tasks
      tasks
    },
    strict: process.env.DEV,
    plugins: [authPersist.plugin,appPersist.plugin]
  })
}