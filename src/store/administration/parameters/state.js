export default function () {
  return {
    item: null,
    items: [],
    form: formData(),
    baseUrl: '/administration/parameters',
  }
}

export function formData() {
  return {
    id: null,
    code: null,
    value: null,
    description: null
  }
}
