import {buildMenu} from "src/helpers/utilities";
import {applicationRoutes} from "src/router/routes";
import avatars from "./avatar";

export default function () {
  return {
    defaultTheme: 'bootstrap-dark-ui-theme',
    menus: buildMenu(applicationRoutes),
    drawer: false,
    header: 'Menu',
    applicationTitle: 'QUASAR BOILERPLATE',
    defaultPagination: {
      sortBy: 'created_at',
      descending: false,
      page: 1,
      rowsPerPage: 10,
    },
    ...avatars
    }
}
