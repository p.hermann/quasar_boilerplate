export function setItem (state, payload) {
  state.item = payload
}

export function setItems (state, payload) {
  state.items = payload
}
