export default function () {
  return {
    item: null,
    items: [],
    form: formData(),
    baseUrl: '/administration/roles',
  }
}

export function formData() {
  return {
    "id": null,
    "slug": null,
    "name": null,
    "description": null,
    "permissions": [],
  }
}
