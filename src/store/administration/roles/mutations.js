export function setItems (state, payload) {
  state.items = payload
}

export function setItem (state, payload) {
  state.item = payload
}
