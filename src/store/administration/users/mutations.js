import {resetObjectAttributes} from 'src/helpers/utilities'
import {formData} from './state'
export function setItem (state, payload) {
  state.item = payload
}

export function setList (state, payload) {
  state.items = payload
}

export function resetForm (state, payload) {
  state.form = resetObjectAttributes(state.form)
}

export function clearForm(state){
  state.form = formData();
}

