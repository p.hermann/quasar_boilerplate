export default function () {
  return {
    loading: false,
    status: null,
    token: null,
    refreshToken: null,
    user: null,
    token_expires: [],
    roles: [],
    permissions: [],
    currentCompany: null,
    companies: []
  }
}
