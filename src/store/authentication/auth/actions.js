import api from 'src/repositories/authentication/auth'
import {helpers} from "boot/helper.boot";

export function login({commit,dispatch}, payload) {
  return api.login(payload).then(async ({data}) => {
    const {
      roles,
      permissions,
      user,
      token: {
        access_token: token,
        expires_in: token_expires,
        refresh_token: refreshToken
      },
      companies
    } = data;

    if(user.avatar_id)
      user.avatar = await dispatch('resources/download',user.avatar_id,{root: true})
        .then(async (file) => {
          console.log(file);
          const imageUrl = await helpers.createUrlBase64(file);
          console.log(imageUrl);
          return imageUrl;
        }).catch((err) =>{
          console.error(err);
          return null
        })

    commit('setAuth', {user, token, refreshToken, roles,
      permissions,token_expires, companies});

    return data
  })
}

export function logout(context, payload) {
  api.logout(payload)
  return context.commit('logout')
}

export function changePassword(context, payload) {
  return api.changePassword(payload)
}
export function resetPassword(context, payload) {
  return api.resetPassword(payload)
}


export function forgottenPasswordRequest(context, payload) {
  return api.forgottenPasswordRequest(payload)
}

