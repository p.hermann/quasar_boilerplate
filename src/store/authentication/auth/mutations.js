export function setAuth (state, payload) {
  state.token = payload.token
  state.refreshToken = payload.refreshToken
  state.user = payload.user
  state.roles = payload.roles
  state.token_expires = payload.token_expires
  state.permissions = payload.permissions
  state.companies = payload.companies;
  // assignation par default d'une filliale
  const [company] = payload.companies;
  state.currentCompany = state.currentCompany||company;
}

export function logout (state, payload) {
  state.token = ''
  state.refreshToken = ''
  state.user = {}
  state.roles = []
  state.permissions = []
  state.companies = []
}
export function changeCompany(state,company){
  return state.currentCompany = company;
}

export  function setUserCompanies(state,companies){
  return state.userCompanies = companies;
}

