
export function storeFilter (store,action,searchable, filter={}) {
  return  (search, update, abort) => {
    update(() => {
      return  store.dispatch(action, {
        pagination: store.state.application.defaultPagination,
        filter: {
          type: "operator",
          operand: "and",
          value: [
            {
              type: 'operator',
              operand: "or",
              'value': [
                ...searchable.map(x => {
                    if(typeof x === "string"){
                      x = {field: x};
                    }
                    return {
                      type: "condition",
                      field: x.field,
                      value: search,
                      operand: x.op || "contain"
                    }
                  }
                )
              ]},
            filter
          ]
        }})
        .catch((err) => abort())
    })
  }
}
