import reduce from "lodash/reduce";
import takeAt from "lodash/get";
import  omit from "lodash/omit";
import snakeCase from "lodash/snakeCase";
import camelCase from "lodash/camelCase";
import { format, copyToClipboard } from 'quasar'

const {humanStorageSize,capitalize}  = format;

export function mergeRoute(...args) {
  return args.flat();
}

export function buildMenu(routes) {

  return reduce(routes, (acc, route) => {
    let menu;
    const menuName = takeAt(route, 'meta.menu');
    if (menuName) {
      menu = {
        title: menuName,
        icon: takeAt(route, 'meta.icon', undefined),
        subTitle: takeAt(route, 'meta.subTitle', undefined),
        roles: takeAt(route, 'meta.roles', []),
        permissions: takeAt(route, 'meta.permissions', []),
        slug: route.name,
        link: {name: route.name}
      }
      acc.push(menu);
    }

    if (route.children) {
      const submenu = buildMenu(route.children);
      if (submenu.length > 0) {
        if (!menu) {
          acc = acc.concat(submenu);
        } else {
          menu.leaf = false;
          menu.subItems = submenu;
        }
      }
    }
    return acc;
  }, []);
}




export function resetObjectAttributes(targetObject) {
  for (let key in targetObject) {
    if (typeof targetObject[key] === 'array') {
      targetObject[key] = []
    } else {
      targetObject[key] = null
    }
  }
  return targetObject
}


export  function createUrlBase64(file) {
  const reader = new FileReader();
  return new Promise(function (resolve,reject){
    reader.addEventListener('load', (e) =>{
      resolve(e.target.result);
    })
    reader.addEventListener('error', (error) => {
      reject(error);
    })
    reader.readAsDataURL(file)
  })
}

export function parse(data) {
  const OPERANDS = {
    'in': '$in',
    'neq': '$ne',
    'not-equal': '$ne',
    'equal': '$eq',
    '!=': '$ne',
    '<>': '$ne',
    'eq': '$eq',
    '=': '$eq',
    'like': '$contL',
    '%': '$contL',
    'contains': '$contL',
    'contain': '$contL',
    'start': '$starts',
    'end': '$ends',
    'ends': '$ends',
    'gt':'$gt',
    'gte':'$gte',
    'lt':'$lt',
    'lte':'$lte',
  }
  if (!data) {
    return null;
  } else if (data.type === 'operator') {
    return {[`$${data.operand}`]: data.value.map(raw => parse(raw))}
  } else {
    return {[data.field]: {[OPERANDS[data.operand]]: data.value}}
  }
}

export function sanitize(payload) {
  let res = {};
  if (!payload) {
    return payload
  }
  if (payload.filter) {
    res.s = parse(payload.filter);
  }
  if (payload.pagination) {
    const {page, rowsPerPage, descending, sortBy} = payload.pagination;
    res.page = page;
    if (rowsPerPage)
      res.limit = rowsPerPage;
    if (sortBy) res.sort = `${sortBy},${descending ? 'DESC' : 'ASC'}`;
  }
  if (payload.populates)
    res.join = payload.populates

  if (payload.join)
    res.join = payload.join

  return res
}

export  {
  humanStorageSize,
  capitalize,
  copyToClipboard,
  takeAt,
  camelCase,
  snakeCase,
  omit,
}

