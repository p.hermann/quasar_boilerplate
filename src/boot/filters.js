import moment from  'moment-timezone'

export default ({ Vue }) => {
  Vue.filter('formatDate', function (value, format = 'DD-MM-YYYY') {
    return moment(value).tz(moment.tz.guess()).format(format)
  })

  Vue.filter('formatDateTime', function (value, format = 'DD-MM-YYYY HH:mm  ') {
    return moment(value).tz(moment.tz.guess()).format(format)
  })
}

