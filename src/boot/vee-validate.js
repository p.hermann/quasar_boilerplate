
import {ValidationProvider, ValidationObserver, extend} from 'vee-validate'
import {confirmed, email, numeric, required, min} from "vee-validate/dist/rules";

export default ({ app, router, Vue, store }) =>{

  Vue.component('ValidationProvider',ValidationProvider);
  Vue.component('ValidationObserver',ValidationObserver);
  extend('email', {...email, message: `Veuillez saisir une adresse mail valide`});
  extend('required', {...required, message: `Ce champ est requis`});
  extend('confirmed', {...confirmed, message: `Les champs ne sont pas identiques`});
  extend('numeric', {...numeric, message: `Les champs doit être un entier`});
  extend('min', {...min});
}
