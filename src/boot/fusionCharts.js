import VueFusionCharts from 'vue-fusioncharts';
import FusionCharts from 'fusioncharts';
import Charts from 'fusioncharts/fusioncharts.charts';
import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion'

export default ({Vue}) => {
  Vue.use(VueFusionCharts, FusionCharts, Charts, FusionTheme)
}
