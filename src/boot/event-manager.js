import Vue from "vue";

const eventManager =  new Vue({name: 'EventManager'});


export default ({app,Vue, store, router}) => {
  Vue.prototype.$eventManager = eventManager;
  eventManager.$parent = app;
}

export {eventManager}
