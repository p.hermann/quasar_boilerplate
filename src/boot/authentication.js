export default async ({urlPath, store,router}) => {

  const hasToken = !!store.getters['auth/getToken']

  if (!hasToken) {
    return router.push('/login')
  }

  if (urlPath.startsWith('/login')) {
    return router.push('/login')
  }
}
