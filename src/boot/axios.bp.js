import axios from 'axios'
import moment from 'moment-timezone'

const http = axios.create({
  baseURL: JSON.parse(process.env.API_URL),
  headers: {'Accept': 'application/json', 'timezone': moment.tz.guess()},
});

export default ({Vue, store, router}) => {
  http.interceptors.request.use(function (config) {
    if (store.state.auth.token) {
      config.headers['Authorization'] = 'Bearer ' + store.state.auth.token;
    }
    return config;
  })

  // http.interceptors.response.use(
  //   }
  // )

  // Vue.prototype.$http = http;
}

export {http}
