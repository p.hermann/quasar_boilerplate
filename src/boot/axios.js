import axios from 'axios'
import {eventManager} from './event-manager'
import moment from 'moment-timezone'

const http = axios.create({
  baseURL: JSON.parse(process.env.API_URL),
  headers: {'Accept': 'application/json', 'timezone': moment.tz.guess(),'x-app':'backoffice'}
});
export default ({ app, router, Vue, store }) => {
  Vue.prototype.$http = http;
  // to fetch data
  Vue.prototype.$request = axios;
  http.interceptors.request.use(function (config) {
    if(store.state.auth.token){
      config.headers['Authorization'] = 'Bearer ' + store.state.auth.token;
    }
    return config;
  }, function (errorHttp) {
    eventManager.$emit('http:connexion-fail', {
      message: errorHttp.message,
      status: 'SERVICE_INTERNET_UNAVAILABLE'
    });
    throw  errorHttp;
  });

  http.interceptors.response.use(function(response){
    return response;
  }, function(error){

    /** @type {import(AxiosResponse)} response */
    const {response}  = error
    if(response){
      switch (response.status){
        case 401:
          eventManager.$emit('http:unauthorized',response);
          break;
        case 403:
          eventManager.$emit('http:access-denied',response);
          break;
        case 404:
          eventManager.$emit('http:not-found',response);
          break;
        case 500:
          eventManager.$emit('http:server-error');
          break;
        default:
          eventManager.$emit('http:error',response);
      }
    } else {
      eventManager.$emit('http:connexion-fail', {
        message: error.message,
        status: 'SERVICE_INTERNET_UNAVAILABLE'
      });
    }
    throw error;
  })
}

export {http};


