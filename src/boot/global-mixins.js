import ErrorHandler from 'src/mixins/errors-handler'
import Authorizations from 'src/mixins/authorizations'
import Helpers from 'src/mixins/helpers'

export default async ({router, Vue, app, urlPath}) =>{
  Vue.mixin(ErrorHandler)
  Vue.mixin(Helpers)
  Vue.mixin(Authorizations)
}

