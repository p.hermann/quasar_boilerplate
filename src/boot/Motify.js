import {Loading, Notify, QSpinnerIos} from 'quasar'

export default async ({router, Vue, app, urlPath}) => {

  function getNotificationDetails(notificationType) {
    switch (notificationType) {
      case 'warning':
        return {
          color: 'orange',
          icon: 'la la-exclamation-triangle',
          textColor: 'white',
        }
        break
      case 'success':
        return {
          color: 'positive',
          icon: 'la la-check-double',
          textColor: 'white',
        }
        break
      case 'error':
        return {
          color: 'negative',
          icon: 'la la-times',
          textColor: 'white',
        }
        break
    }
  }

  /**
   *
   * @param type - This is the type of the notification that you need to throw
   * Possible values are ( error, warning & success)
   * @param message - The message attribute is the section of text that you want to display
   * @param redirectUrl - If you need to redirect to a specific url after the notification, you can
   * give that url or route object here
   */
  function showNotif(type,
                     message,
                     redirectUrl = null,
                     duration = 1000,
                     html = false,
                     position = 'top-right') {
    let details = getNotificationDetails(type)

    Notify.create({
      progress: true,
      color: details.color,
      textColor: details.textColor,
      icon: details.icon,
      classes: 'glossy',
      html, message,
      position: position || 'bottom-right',
      multiLine: false,
      // actions: [
      //   { label: 'Ok', color: 'white', handler: () => { /* ... */ } }
      // ],
      timeout: duration || 1000,
      onDismiss: function () {
        if (redirectUrl) {
          router.push(redirectUrl)
        }
      },
    })
  }

  function showLoader() {
    const spinner = QSpinnerIos
    Loading.show({
      spinner,
      spinnerColor: '#2d2b2b',
      spinnerSize: 140,
      backgroundColor: '#ade7a6',
      message: 'Veuillez patienter...',
      messageColor: '#2d2b2b',
      customClass: 'mySpinner',
    })
  }

  function hideLoader() {
    Loading.hide()
  }

  Vue.prototype.Motify = showNotif
  Vue.prototype.Loader = {
    show: showLoader,
    hide: hideLoader,
  }

}

