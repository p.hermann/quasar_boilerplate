import * as  helpers from '../helpers'

export default ({ app,Vue }) => {
  Vue.prototype.$helpers = helpers
}

export { helpers }
