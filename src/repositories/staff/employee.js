import {http} from 'boot/axios'
const baseUri = 'api/v1/staffs/employees'

export default {
  index(payload) {
    return http.get(`${baseUri}/`, { params: {...payload},})
      .then((result) => result)
  },

  show(payload, params) {
    return http.get(`${baseUri}/${payload.id}`, {params})
      .then((result) => result.data)
  },

  create(payload) {
    return http.post(`${baseUri}`, payload)
      .then((result) => result.data)
  },

  update(payload) {
    return http.patch(`${baseUri}/${payload.id}`, payload)
      .then((result) => result.data)
  },
  showCurrentContract(payload) {
    return http.get(`api/v1/staffs/employees/${payload.id}/current-contract`, payload)
      .then((result) => result.data)
  },

  destroy(payload) {
    return http.delete(`${baseUri}/${payload.id}`, payload)
  },


  saveNationalities(payload) {
    return http.put(`${baseUri}/${payload.id}/nationalities/sync`, payload)
      .then((result) => result.data)
  },
  saveResources(payload) {
    return http.put(`${baseUri}/${payload.id}/resources/sync`, payload)
      .then((result) => result.data)
  },

}
