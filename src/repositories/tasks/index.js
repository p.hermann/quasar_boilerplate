import {http} from 'boot/axios'

export default {
  index(payload) {
    return http.get('api/v1/tasks')
      .then((result) =>  result.data)
  }
}
