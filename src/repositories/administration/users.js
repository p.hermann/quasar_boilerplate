import {http} from 'boot/axios'
const baseUri = 'api/v1/users'

export default {
  index(payload) {
    return http.get(`${baseUri}/`, { params: {...payload},})
      .then((result) => result)
  },

  show(payload, params) {
    return http.get(`${baseUri}/${payload.id}`, {params})
      .then((result) => result.data)
  },

  create(payload) {
    return http.post(`${baseUri}`, payload)
      .then((result) => result.data)
  },

  update(payload) {
    return http.patch(`${baseUri}/${payload.id}`, payload)
      .then((result) => result.data)
  },

  destroy(payload) {
    return http.delete(`${baseUri}/${payload.id}`, payload)
  },
  changePassword(payload) {
    return http.patch(`${baseUri}/change-password/${payload.id}`, payload)
  },
  attachRoles(payload) {
    return http.put(`${baseUri}/${payload.id}/roles/sync`, payload)
  },
  attachCompanies(payload) {
    return http.put(`${baseUri}/${payload.id}/companies/sync`, payload)
  },
  attachPermissions(payload) {
    return http.put(`${baseUri}/${payload.id}/permissions/sync`, payload)
  },
  activeOrDisable(payload) {
    return http.put(`${baseUri}/revoked_access`, payload)
  },
  forceNewPassword(payload) {
    return http.put(`${baseUri}/force-change-password`, payload)
  },
}
