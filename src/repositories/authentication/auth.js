import {http} from 'boot/axios'

export default {
  login(payload) {
    return http.post('api/v1/auth/login', payload)
      .then((result) =>  result.data)
  },

  logout(payload) {
    delete http.defaults.headers.common['Authorization']
  },

  changePassword(payload) {
    return http.post(`api/v1/auth/change-password/`, payload)
      .then((result) =>  result.data)
  },
  resetPassword(payload) {
    return http.post(`api/v1/auth/reset-password/`, payload)
  },

  forgottenPasswordRequest(payload) {
    return http.post(`api/v1/auth/reset-password-request/`, payload)
  },

}
