import {http} from 'boot/axios'

export default {

  upload(payload) {
    return http.post(`api/v1/resources/upload`, payload)
      .then((result) => result.data);
  },
  download(id) {
    return http.get(`api/v1/resources/download/${ id }`, {
      responseType: 'blob'
    })
      .then((result) => result.data);
  },
  remove(id){
    return http.delete(`api/v1/resources/destroy/${ id }`);
  }
}
