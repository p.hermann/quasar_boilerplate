
export default {
  props: {
    mode:{
      type: String,
      validate: (val) => ['view','create','update'].includes(val)
    },
  }
}
