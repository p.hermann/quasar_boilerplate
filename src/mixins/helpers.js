import moment from 'moment-timezone'
import currency from 'currency.js'

export default {

  methods: {
    _formatDate(date, format = 'YYYY-MM-DD HH:mm') {
      if (date) {
        return moment(date).tz(moment.tz.guess()).format(format);
      }
      return date
    },

    _formatAmount(amount, decimal = ',', separator = ' ') {
      if (amount && (typeof amount === 'number' || typeof amount === 'string'))
        return currency(amount, {symbol: '', decimal, separator}).format()
      return amount
    },
  }
};
