import get from 'lodash/get'

export default {

  methods: {
    _getErrorMessage(error) {
      const message = get(error, 'response.data.message', '') || ''
      switch (get(error, 'response.data.status', undefined) || undefined) {

        case 'FACTORY_NOT_FOUND':
          return 'Votre usine est introuvable ou indéfinie. ' + message

        case 'MARKAGE_NOT_FOUND':
          return 'Aucun marquage trouvé pour cette combinaison : ' + message

        case 'INVOICE_ID_NOT_UPDATED':
          return message

        case 'WEIGHT_BRIDGE_OPERATION_NOT_FOUND':
          return message

        case 'INVALID_FORM_USER':
          return 'Les informations du formulaire sont incorrects. Verifier par exemple l\'unicité du ' +
            'login et les autres champs. ' + message

        case '22_P_02':
          return message
        case '23505':
          return 'Verifier les doublons dans les données envoyées.'
        case 'ACCESS_DENIED':
          return 'Vous n\'êtes pas autorisé à accéder à cette ressource : ' + message

        default:
          return 'Une erreur est survenue. ' + message
      }
    },
  }
};
