import {mapState} from "vuex";

export default{
  methods: {
    _can(slug){
      if(!this._permissions) {
        return false
      }
      return (this._permissions.some(element => element === slug) )
    },
    _is(role){
      return (this._roles.find(element => element === role) )
    },

  },

  computed: {
    ...mapState({
      _permissions: state => state.authentication.permissions,
      _roles: state => state.authentication.roles
    })
  }
}
