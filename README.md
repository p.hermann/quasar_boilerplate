# GROUP GRH  (frontoffice-web)

Les ressources humaines tiennent un rôle primordial en entreprise.
Le service ressources humaines a une haute responsabilité dans la réussite de l’entreprise,
car il assure plusieurs fonctions décisives : recrutement, relations sociales, formation professionnelle, etc.
Les missions sont vastes et sont reconnues comme le principal levier de développement de l’entreprise,
car l’humain y incarne aujourd’hui un véritable enjeu stratégique.Dans cette optique une application a été demande
pour la gestion de cette dernière

## Comment lancer le projet ?
### Installation des dependances
```bash
npm install
```

### démarrer le developpement
```bash
quasar dev
```
### Mise en production
```bash
quasar build
```

### Comment costomiser les l'application
Aller sur [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).

## Configuration

Nous utilison pour les icons [line-awesome](https://icons8.com/line-awesome)


### Ce qui faut savoir sur GRH

Elle a deux composants primondiales pour les entrées:
MSelect,
MField(text, date , date_time, time,file)
pageContainer

#### MSelect
Pour les choix dans une liste d'élément. Fait les rechere sur le store

#### MField
Elle  elle de multipe fonction selon le ``input-type`` précisé.
nous avons comme ``input-type``: text, date , date_time, time,file

#### PageContainer
Assure le gestion du contenu de l'application.
C'est à dire le partie centrale de l'application.
Permet le defilement de la page.
Besoin fortement de correction

