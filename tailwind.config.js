module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        'sans': 'var(--font-family)',
      },
      backgroundColor: {
        skin: {
          fill: 'var(--primary-color)'
        }
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
