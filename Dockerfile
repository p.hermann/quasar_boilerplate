# develop stage
#The FROM instruction initializes a new build stage and sets the Base Image for subsequent instructions
FROM node:12-alpine as develop-stage

RUN apk add --no-cache g++ gcc libgcc libstdc++ make python  git

#RUN npm install --quiet node-gyp -g
RUN  npm install -g @quasar/cli

# copy project files and folders to the current working directory (i.e. 'app' folder)
WORKDIR /app

#EXPOSE 8080
ADD package.json .
ADD package-lock.json .

#Instal the nodes dependencies
RUN npm install

#Copy all files in /app folder
COPY . /app

RUN quasar build


# production stage
FROM nginx:stable as production-stage
COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=develop-stage /app/dist/spa /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
